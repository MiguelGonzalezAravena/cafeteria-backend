//File: controllers/producto.controller.js

var mongoose = require('mongoose');  
var Producto = mongoose.model('Producto');

// GET - Obtener todos los productos
exports.obtenerProductos = function(req, res) {  
  Producto.find(function(err, producto) {
    if(err) {
      res.send(500, err.message);
    }
    res.status(200).jsonp({producto: producto});
  });
};

// POST - Agregar un nuevo producto
exports.agregarProducto = function(req, res) {
  if(typeof req.body.nombre == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener un nombre.'});
  } else if(typeof req.body.cantidad == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener una cantidad.'});
  } else if(typeof req.body.precio == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener un precio.'});
  } else if(typeof req.body.categoria == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener una categoría.'});
  } else if(typeof req.body.estado == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener un estado.'});
  } else if(typeof req.body.hora_inicio == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener una hora de inicio.'});
  } else if(typeof req.body.hora_fin == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener una hora de fin.'});
  } else if(typeof req.body.imagen == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'El producto debe contener una imagen.'});
  }

  var producto = new Producto({
    nombre: req.body.nombre,
    cantidad: req.body.cantidad,
    precio: req.body.precio,
    categoria: req.body.categoria,
    estado: req.body.estado,
    hora_inicio: req.body.hora_inicio,
    hora_fin: req.body.hora_fin,
    imagen: req.body.imagen,
    _id: mongoose.Types.ObjectId()
  });

  producto.save(function(err, producto) {
    if(err) {
      return res.status(500).send(err.message);
    }
    res.status(200).jsonp(producto);
  });
};