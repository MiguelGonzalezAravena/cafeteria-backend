//File: controllers/combo.controller.js

var mongoose = require('mongoose');  
var Pedido = mongoose.model('Pedido');

// GET - Obtener todos las pedidos
exports.obtenerPedidos = function(req, res) {  
  Pedido.find(function(err, receta) {
    if(err) {
      res.send(500, err.message);
    }
    res.status(200).jsonp(receta);
  });
};

// GET - Obtener un pedido por ID
exports.buscarID = function(req, res) {  
  Pedido.findById(req.params.id, function(err, pedido) {
    if(err) {
      return res.status(500).send(err.message);
    }
    res.status(200).jsonp(pedido);
  });
};

// POST - Agregar un nuevo pedido
exports.agregarPedido = function(req, res) {  
  var pedido = new Pedido({
    usuario: req.body.usuario,
    precio: req.body.precio,
    imagen: req.body.imagen,
    sucursal: req.body.imagen,
    productos: req.body.productos,
    _id: mongoose.Types.ObjectId()
  });

  pedido.save(function(err, pedido) {
    if(err) {
      return res.status(500).send(err.message);
    }
    res.status(200).jsonp(pedido);
  });
};
