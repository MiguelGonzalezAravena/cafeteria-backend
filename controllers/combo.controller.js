//File: controllers/combo.controller.js

var mongoose = require('mongoose');  
var Combo = mongoose.model('Combo');

//GET - Obtener todas las recetas
exports.buscarRecetas = function(req, res) {  
  Receta.find(function(err, receta) {
    console.log('GET /recetas');
    if(err) {
      res.send(500, err.message);
    }
    res.status(200).jsonp(receta);
  });
};

//GET - Obtener todas las recetas según ingredientes
exports.buscarRecetasIngredientes = function(req, res) {  
  var mostrar = [];
  Receta.find(function(err, receta) {
    console.log('GET /recetas');
    if(err) {
      res.send(500, err.message);
    }
    console.log(req.body.ingredientes);
    for (var i = receta.length - 1; i >= 0; i--) {
      if((req.body.ingredientes).compare(receta[i].ingredientes)) {
        mostrar.push(receta[i]);
      }
    }
    console.log(mostrar);
    res.status(200).jsonp(mostrar);
    //res.status(200).jsonp(receta);
  });
};

//GET - Obtener una receta por ID
exports.buscarID = function(req, res) {  
  Receta.findById(req.params.id, function(err, receta) {
    if(err) {
      return res.status(500).send(err.message);
    }

    console.log('GET /receta/' + req.params.id);
    res.status(200).jsonp(receta);
  });
};

//POST - Agregar una nueva receta
exports.agregarReceta = function(req, res) {  
  console.log('POST');
  console.log(req.body);

  var receta = new Receta({
    titulo: req.body.titulo,
    imagen: req.body.imagen,
    autor: req.body.autor,
    ingredientes: req.body.ingredientes,
    cuerpo: req.body.cuerpo
  });

  receta.save(function(err, receta) {
    if(err) {
      return res.status(500).send(err.message);
    }
    res.status(200).jsonp(receta);
  });
};

// POST - Puntuar una nueva receta
exports.puntuarReceta = function(req, res) {
  console.log('POST /receta/puntuar/');
  console.log(req.body);
  var dataError = { message: 'Ya has puntuado esta receta.'};
  var nuevoPuntaje = {
    puntaje: req.body.puntaje,
    ip: req.connection.remoteAddress
  };

  Receta.findById(req.body._id, function(err, receta) {
    for (var i = receta.puntajes.length - 1; i >= 0; i--) {
      if(receta.puntajes[i].ip == req.connection.remoteAddress) {
        return res.status(500).send(dataError);
      } else {
        Receta.findByIdAndUpdate(req.body._id, { $addToSet: { puntajes: nuevoPuntaje }}, function (err, receta) {
          if (err) {
            return res.status(500).send(err.message);
          }
          res.status(200).jsonp(receta);
        });
        
      }
    }
  });
};

// GET - Conteo de recetas según autor
exports.obtenerTotalRecetas = function(req, res) {
  console.log('GET /recetas/autor/' + req.params.name + '/' + (typeof req.params.type != 'undefined' ? req.params.type : ''));
  if(req.params.type == 'total') {
    Receta.where('autor', req.params.name).count(function (err, count) {
      if (err) {
        return res.status(500).send({ message: 'Autor no encontrado' });
      }
      console.log(count);
      var totalObject = {total: count};
      if(count >= 0) {
        res.status(200).jsonp(totalObject);
      }
    });
  } else {
    Receta.find({ 'autor' : req.params.name }, function(err, receta) {
      res.status(200).jsonp(receta);
    });
  }
};