/*
* @Author: Miguel González Aravena
* @Email: miguel.gonzalez.93@gmail.com
* @Github: https://github.com/MiguelGonzalezAravena
* @Date: 2016-11-28 10:52:37
* @Last Modified by: Miguel
* @Last Modified time: 2016-12-12 01:15:15
*/
//File: controllers/sucursal.controller.js
var mongoose = require('mongoose');  
var Sucursal = mongoose.model('Sucursal');

// GET - Obtener todas las sucursales
exports.obtenerSucursales = function(req, res) {  
  Sucursal.find(function(err, sucursal) {
    if(err) {
      res.send(500, err.message);
    }
    res.status(200).jsonp({sucursal: sucursal});
  });
};

// POST - Agregar una nueva sucursal
exports.agregarSucursal = function(req, res) {
  if(typeof req.body.nombre == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'La sucursal debe tener un nombre.'});
  } else if(typeof req.body.encargado == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'La sucursal debe tener un encargado.'});
  } else if(typeof req.body.longitud == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'La sucursal debe estar en una longitud.'});
  } else if(typeof req.body.latitud == 'undefined') {
    return res.status(500).jsonp({ mensaje: 'La sucursal debe estar en una latitud.'});
  }

  var sucursal = new Sucursal({
    nombre: req.body.nombre,
    encargado: req.body.encargado,
    ubicacion: [req.body.longitud, req.body.latitud],
    _id: mongoose.Types.ObjectId()
  });

  sucursal.save(function(err, sucursal) {
    if(err) {
      return res.status(500).send(err.message);
    }
    res.status(200).jsonp(sucursal);
  });
};