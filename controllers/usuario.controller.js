//File: controllers/usuario.controller.js

var mongoose = require('mongoose');  
var Usuario = mongoose.model('Usuario');

// POST - Acceder a cuenta
exports.accederUsuario = function(req, res) {
  var token = req.body.token;

  Usuario.findOne({
    token: token
  }, 'token', function(err, usuario) {
    if(err) {
      res.send(500, err.message);
    }
    res.status(200).json(usuario);
  });
};

// POST - Crear usuario
exports.crearUsuario = function(req, res) {
  var token = req.body.token  
  ;

  var usuario = new Usuario({
    token: req.body.token,
    imagen: req.body.imagen,
    autor: req.body.autor,
    ingredientes: req.body.ingredientes,
    cuerpo: req.body.cuerpo
  });

  usuario.save(function(err, receta) {
    if(err) {
      return res.status(500).send(err.message);
    }
    res.status(200).jsonp(receta);
  });
};
  /*
  Usuario.find(function(err, receta) {
    console.log('GET /login');
    if(err) {
      res.send(500, err.message);
    }
    res.status(200).jsonp(receta);
  });
  */
