var express = require('express')
 , cors = require('cors')
 , app = express()
 , bodyParser = require('body-parser')
 , methodOverride = require('method-override')
 , mongoose = require('mongoose')
 , port = process.env.PORT || 3000
;

mongoose.connect('mongodb://heroku_p3btmb48:gthrd7pkrrmd93tpbhh25ro0ok@ds111788.mlab.com:11788/heroku_p3btmb48', function(err, res) {
  if(err) {
    console.log('ERROR: connecting to Database. ' + err);
  } else {
    console.log('Connected to "heroku_p3btmb48" database');
  }
});

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cors());

var modelCarta = require('./models/carta')(app, mongoose);
var modelCombo = require('./models/combo')(app, mongoose);
var modelPedido = require('./models/pedido')(app, mongoose);
var modelProducto = require('./models/producto')(app, mongoose);
var modelSucursal = require('./models/sucursal')(app, mongoose);
var modelUsuario = require('./models/usuario')(app, mongoose);
var comboCtrl = require('./controllers/combo.controller');
var pedidoCtrl = require('./controllers/pedido.controller');
var productoCtrl = require('./controllers/producto.controller');
var usuarioCtrl = require('./controllers/usuario.controller');
var sucursalCtrl = require('./controllers/sucursal.controller');

var router = express.Router();
router.get('/', function(req, res) {
  res.send('Hello world!');
});
app.use(router);

var r = express.Router();

// GET /login
r.route('/login')
  .post(usuarioCtrl.accederUsuario);

// GET /productos
r.route('/productos')
  .get(productoCtrl.obtenerProductos)
  .post(productoCtrl.agregarProducto);

// GET /pedidos
r.route('/pedidos')
  .get(pedidoCtrl.obtenerPedidos)
  .post(pedidoCtrl.agregarPedido);

// GET /sucursales
r.route('/sucursales')
  .get(sucursalCtrl.obtenerSucursales)
  .post(sucursalCtrl.agregarSucursal);

/*
receta.route('/recetas')  
  .get(RecetaCtrl.buscarRecetas)
  .post(RecetaCtrl.agregarReceta);

receta.route('/recetas/buscar')
  .post(RecetaCtrl.buscarRecetasIngredientes);

receta.route('/receta/:id')  
  .get(RecetaCtrl.buscarID);

receta.route('/receta/puntuar')
  .post(RecetaCtrl.puntuarReceta);

receta.route('/recetas/autor/:name/:type')
  .get(RecetaCtrl.obtenerTotalRecetas);

receta.route('/recetas/autor/:name')
  .get(RecetaCtrl.obtenerTotalRecetas);

*/
app.use('/api', r);
app.listen(port, function() {
  console.log('Node server running on http://cafeteria-app.herokuapp.com/');
});