/*
* @Author: Miguel González Aravena
* @Email: miguel.gonzalez.93@gmail.com
* @Github: https://github.com/MiguelGonzalezAravena
* @Date: 2016-11-28 10:52:37
* @Last Modified by: Miguel
* @Last Modified time: 2016-12-12 01:07:16
*/
var mongoose = require('mongoose')
 , Schema = mongoose.Schema
;

// Esquema de sucursal
var sucursalSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  nombre: {type: String},
  encargado: {type: String},
  ubicacion: {
    type: [Number],
    index: '2d'
  }
});

module.exports = mongoose.model('Sucursal', sucursalSchema);
