var mongoose = require('mongoose')
 , Schema = mongoose.Schema
;

// Esquema de combo
var comboSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  nombre: {type: String},
  productos: [{
    nombre: {type: String},
    cantidad: {type: Number},
    precio: {type: Number},
    estado: {type: Number},
    hora_inicio: {type: String},
    hora_fin: {type: String}
  }],
  estado: {type: Number},
  hora_inicio: {type: String},
  hora_fin: {type: String},
  precio: {type: Number},
  imagen: {type: String}
});

module.exports = mongoose.model('Combo', comboSchema);