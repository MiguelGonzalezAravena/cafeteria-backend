var mongoose = require('mongoose')
 , Schema = mongoose.Schema
;

var recetaSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  autor: {type: String},
  fecha: {
    type: Date,
    default: Date.now 
  },
  titulo: {type: String},
  cuerpo: {type: String},
  ingredientes: [{type: String}],
  puntajes: [{
    puntaje: {type: Number},
    fecha: {type: Date, default: Date.now},
    ip: {type: String}
  }],
  imagen: {type: String}
});

module.exports = mongoose.model('Receta', recetaSchema);
