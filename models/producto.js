/*
* @Author: Miguel González Aravena
* @Email: miguel.gonzalez.93@gmail.com
* @Github: https://github.com/MiguelGonzalezAravena
* @Date: 2016-11-14 10:48:34
* @Last Modified by: Miguel
* @Last Modified time: 2016-11-28 00:52:47
*/
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// Esquema de producto
var productoSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  nombre: {type: String},
  cantidad: {type: Number},
  precio: {type: Number},
  categoria: {type: String},
  estado: {type: Number},
  hora_inicio: {type: String},
  hora_fin: {type: String},
  imagen: {type: String}
});

module.exports = mongoose.model('Producto', productoSchema);