var mongoose = require('mongoose')
 , Schema = mongoose.Schema
;

// Esquema de pedido
var pedidoSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  usuario: {type: String},
  precio: {type: Number},
  imagen: {type: String},
  sucursal: {type: String},
  productos: [{
    nombre: {type: String},
    cantidad: {type: Number},
    precio: {type: Number}
  }],
  fecha_pedido: {
    type: Date,
    default: Date.now 
  }
});

module.exports = mongoose.model('Pedido', pedidoSchema);
