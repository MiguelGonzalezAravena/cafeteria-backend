var mongoose = require('mongoose')
 , Schema = mongoose.Schema
;

// Esquema de usuario
var usuarioSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  correo: {type: String},
  nombre: {type: String},
  token: {type: String},
  tipo: {type: Number},
  inicio_sesion: {
    type: Date,
    default: Date.now 
  }
});

module.exports = mongoose.model('Usuario', usuarioSchema);
